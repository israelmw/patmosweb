<!--html5 basico-->
<!DOCTYPE html>
<html lang="es">
<head>
	<title>Patmos Produciones</title>
	<meta charset="utf-8"/> 
	<meta name="description" content="En esta secci&oacute;n podr&aacute;s ponerte en contacto con nosotros."/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="shrotcut icon" type="image/x-icon" href="LogoPezicon.ico"/>
	<link rel="author" type="text/plain" href="humans.txt">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,300,600,400' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/estilos.css"/> 
	<link rel="stylesheet" href="css/menu.css"/> 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<script src="funciones.js"></script>
	<script>
		!window.jQuery && document.write("<script src='js/jquery.min.js'><\/script>");
	</script>
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/thunk/html5.js"></script>
	<![endif]-->
	<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-35546410-1']);
  _gaq.push(['_setDomainName', 'patmosproducciones.com.ar']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
	<header>
		<figure> 
			<a href="index.html">
			<img class="fade" alt="PatmosProducciones" src="img/patmos-logo.png"/>
			<figcaption></figcaption>
			</a>
		</figure>
		<h1>Diseño inspirado en la naturaleza de la vida</h1>
	</header>
	<nav>
		<div id='cssmenu'>
			<ul>
  			 <li><a id="img-inicio"href='index.html'><span><img src="img/inicio.png"></span></a></li>
   			<li class='has-sub '><a href='#'><span>Productos</span></a>
    			  <ul>
    			     <li class='has-sub '><a href='#'><span>Diseño</span></a>
    			        <ul>
    			           <li><a href='website.html'><span>Website</span></a></li>
    			           <li><a href='redessociales.html'><span>Redes Sociales</span></a></li>
    			            <li><a href='graficas.html'><span>Graficas</span></a></li>
    			        </ul>
    			     </li>
    			     <li class='has-sub '><a href='#'><span>Multimedia</span></a>
    			        <ul>
    			           <li><a href='videos.html'><span>Videos</span></a></li>
    			           <li><a href='montajes.html'><span>Montajes</span></a></li>
    			        </ul>
     			    </li>
    			  </ul>
  			 </li>
  			  <li class='has-sub '><a href='portfolio.html'><span>Portfolio</span></a></li>
  			 <li><a href='clientes.html'><span>Clientes</span></a></li>
 			  <li><a href='contacto.html'><span>Contacto</span></a></li>
			</ul>
			 
			</div>
	</nav>
	<section id="contenedor">
		<img id="contactanos"src="img/contactanos.png" >
		<section id="contacto">
			<article id="info-contacto">
				<p id="sinsangria">
					Email:
					<span class="datos-contacto">patmosproducciones@gmail.com</span>
					<br />
					M&oacute;vil:
					<span class="datos-contacto">+54 (011) 5099 9958 / +54 (011) 4783 4542 </span>
					<br />
					Web:
					<span class="datos-contacto">patmosproducciones.com.ar</span>
					<br />
					Direcci&oacute;n:
					<span class="datos-contacto">Ciudad de la Paz 2535 PB "2" | Belgrano | Ciudad de Buenos Aires | Argentina</span>
					<br />
					Social Media:
					<br />
					<span class="datos-contacto">
						<a class="fade" href="http://facebook.com/patmosproducciones" title="Facebook" target="_blank"><img src="img/logo-facebook.png" ></a>
						&nbsp;&nbsp;&nbsp;
						<a class="fade" href="http://twitter.com/patmosproduccio" title="Twitter" target="_blank"><img src="img/logo-twitter.png" ></a>
						&nbsp;&nbsp;&nbsp;
						<a class="fade" href="http://vimeo.com/patmos" title="Vimeo" target="_blank"><img src="img/logo-vimeo.png" ></a>
						&nbsp;&nbsp;&nbsp;
						<a class="fade" href="http://youtube.com/patmosproducciones" title="Youtube" target="_blank"><img src="img/logo-youtube.png"></a>
						&nbsp;&nbsp;&nbsp;
					</span>
				</p>
			</article>
			<article id="contatanos">
				<?php
       				if(isset($_POST['boton'])){
            			if($_POST['nombre'] == ''){
            			    $errors[1] = '<span class="error">Ingrese su nombre</span>';
            			}else if($_POST['email'] == '' or !preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/",$_POST['email'])){
            			    $errors[2] = '<span class="error">Ingrese un email correcto</span>';
            			}else if($_POST['asunto'] == ''){
            			    $errors[3] = '<span class="error">Ingrese un asunto</span>';
            			}else if($_POST['mensaje'] == ''){
            			    $errors[4] = '<span class="error">Ingrese un mensaje</span>';
            			}else{
            			    $dest = 'patmosproducciones@gmail.com'; //Email de destino
            			    $nombre = $_POST['nombre'];
            			    $email = $_POST['email'];
            			    $asunto = $_POST['asunto']; //Asunto
            			    $cuerpo .= $_POST['nombre']."<br>";
            			    $cuerpo .= $_POST['email']."<br>";
            			    $cuerpo .= $_POST['asunto']."<br>";
            			    $cuerpo .= $_POST['mensaje']."<br>";
            			    
            			    //Cabeceras del correo
            			    $headers = "From: $nombre $email\r\n"; //Quien envia?
            			    $headers .= "X-Mailer: PHP5\n";
            			    $headers .= 'MIME-Version: 1.0' . "\n";
            			    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; //
 
            			    if(mail($dest,$asunto,$cuerpo,$headers)){
            			        $result = '<div class="result_ok">Email enviado correctamente </div>';
            			        // si el envio fue exitoso reseteamos lo que el usuario escribio:
            			        $_POST['nombre'] = '';
            			        $_POST['email'] = '';
            			        $_POST['asunto'] = '';
            			        $_POST['mensaje'] = '';
            			    }else{
            			        $result = '<div class="result_fail">Hubo un error al enviar el mensaje </div>';
            			    }
           				}
       				}
   				?>
				<form class="contacto" method="POST" action="" >
					<fieldset>
						<legend>Env&iacute;anos tus comentarios</legend>
						<div>
							<label for="nombre">Nombre:</label>
							<br />
							<input type="text" id="nombre" class="nombre" name="nombre" value='<?php echo $_POST['nombre']; ?>'requiered/>
							<?php echo $errors[1]; ?>
						</div>
						<br />
						<div>
							<label for="email">Email:</label>
							<br />
							<input type="email" id="email" class="email" name="email" value='<?php echo $_POST['email']; ?>' requiered/>
							<?php echo $errors[2]; ?>
						</div>
						<br />
						<div>
							<label for="asunto">Asunto:</label>
							<br />
							<input type="text" id="asunto" class="asunto" name="asunto" value='<?php echo $_POST['asunto']; ?>' requiered/>
							<?php echo $errors[3]; ?>
						</div>
						<br />
						<div>
							<label for="mensaje">Mensaje:</label>
							<br />
							<textarea  id="mensaje" class="mensaje" name="mensaje" cols="31" rows="3" value='' requiered><?php echo $_POST['mensaje']; ?></textarea>
							<?php echo $errors[4]; ?>
						</div>
						<div>
							<input type="submit" id="enviar" class="boton" name="boton" value="Envia Mensaje"/>
						</div>
					</fieldset>
				 <?php echo $result; ?>
				</form>
			</article>
		</section>
		<section id="mapa">
			<iframe width="960" height="536" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.ar/maps?f=q&amp;source=s_q&amp;hl=es&amp;geocode=&amp;q=Ciudad+de+la+Paz+2535,+Buenos+Aires&amp;aq=0&amp;oq=ciudad+de+la+paz+2535&amp;sll=-34.592844,-58.416105&amp;sspn=0.047976,0.090895&amp;ie=UTF8&amp;hq=&amp;hnear=Ciudad+de+la+Paz+2535,+Belgrano,+Buenos+Aires&amp;t=m&amp;ll=-34.558597,-58.461084&amp;spn=0.037887,0.082397&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
			<br />
			<small><a href="https://maps.google.com.ar/maps?f=q&amp;source=embed&amp;hl=es&amp;geocode=&amp;q=Ciudad+de+la+Paz+2535,+Buenos+Aires&amp;aq=0&amp;oq=ciudad+de+la+paz+2535&amp;sll=-34.592844,-58.416105&amp;sspn=0.047976,0.090895&amp;ie=UTF8&amp;hq=&amp;hnear=Ciudad+de+la+Paz+2535,+Belgrano,+Buenos+Aires&amp;t=m&amp;ll=-34.558597,-58.461084&amp;spn=0.037887,0.082397&amp;z=14&amp;iwloc=A" style="color:#0000FF;text-align:left">Ver mapa más grande</a></small>
			<br />
		</section>
	</section>
	<section id="sitemap">
		<article class="columnas">
			<span class="secciones-categorias-titulos">PRODUCTOS</span>
			<br><br>
   			<a href="website.html" class="fade">Website</a>
   			<br><br>
   			<a href="redessociales.html" class="fade">Redes Sociales</a>
   			<br><br>
   			<a href="graficas.html" class="fade">Graficas</a>
   			<br><br>
   			<a href="videos.html" class="fade">Videos</a>
   			<br><br>
   			<a href="montajes.html" class="fade">Montajes</a>
   		</article>
  		
  		<article class="columnas">
  			<span class="secciones-categorias-titulos">PORTFOLIO</span>
  			<br><br>
    		<a href="portfolio.html" class="fade">Nuestro trabajos</a>
  		</article>

  		<article class="columnas">
  			<span class="secciones-categorias-titulos">CLIENTES</span>
  			<br><br>
   			<a href="clientes.html" class="fade">Cartas Clientes</a>
  		</article>
  
  		<article class="columnas">
  		 	<span class="secciones-categorias-titulos">CONTACTO</span>
  			<br><br>
    		<a href="contacto.php" class="fade">Contactanos</a>
    	</article>  		
	</section>
	<footer>
		<img id="logo-pie"src="img/logo-pie.png" alt="Logo-Patmos-Pie"/>
		<article id="footer">
		Contacto Patmos Diseño - Teléfonos: (011) 5099-9958 - Mail: patmosproducciones@gmail.com
		<br>
		Política de Privacidad | Servicios Extendidos | Multimedias | Dispositivos Móviles
		<br>
		© 2011-2012 | <a rel="author" href="http://patmosproducciones.com.ar" target="_blank">Patmos Produccioes.</a> All Rights Reserved. 
		</article>
	</footer>

</body>
</html>